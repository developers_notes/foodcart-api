<?php
/**
 * SchoolYear Model | app/Models/SchoolYear.php
 *
 * @author      Jaypee Sanchez <developers_notes@yahoo.com>
 */
namespace App\Models;
use DB;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * Class Model Collection - SchoolYear
 *
 */
class Users extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * Set default model table name.
     *
     * @var string
     * @var status // A-Active, P-Pending, D-Deleted, I-Inactive
     */
    protected $table = 'Users';
    protected $connection = 'mysql';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'login_attempt'
    ];

    protected $hidden = [
        'token',
        'reset_token'
    ];

    public static function getCurrentUser(Request $request = NULL) {
        $token = $request->header('Authorization');
        $token = explode(' ', $token);
        $token = $token[1];
        $user = Users::where('token', '=', $token)->first();
        if (isset($user->id)) {
            return $user;
        } 
    }

    
    /*
    public function tasks() {
        return $this->belongsToMany(Tasks::class, Student_Tasks::class, 'user_id', 'task_id');
    }

    public function roles() {
        return $this->hasOne(UserRoles::class, 'id', 'user_role_id');
    }

    public function lectures() {
        return $this->belongsToMany(Lectures::class, Student_Lectures::class, 'user_id', 'lecture_id');
    }

    public static function getUserTasks($uid = 0) {
        return DB::table('Users')
            ->select(DB::raw('Users.id, Users.username,
            Tasks.id as task_id, Tasks.item_count, Tasks.view_type_id, Tasks.user_id as teacher_id, u2.image as teacher_image,
            Tasks.name as task_name, Tasks.summary, Tasks.date_deadline, Tasks.task_type_id as task_type_id
            '))
            ->leftJoin('Student_Tasks', 'Users.id', '=', 'Student_Tasks.user_id')
            ->leftJoin('Tasks', 'Tasks.id', '=', 'Student_Tasks.task_id')
            ->leftJoin('Users as u2', 'u2.id', '=', 'Tasks.user_id')
            ->where('Student_Tasks.user_id', '=', $uid)
            ->get();
    }

    public static function getUserAdminTasks($uid = 0) {
        return DB::table('Users')
            ->select(DB::raw('Users.id, Users.username,
            Tasks.id as task_id, Tasks.item_count, Tasks.view_type_id, Tasks.user_id as teacher_id, Users.image as teacher_image,
            Tasks.name as task_name, Tasks.summary, Tasks.date_deadline, Tasks.task_type_id as task_type_id
            '))
            //->leftJoin('Student_Tasks', 'Users.id', '=', 'Student_Tasks.user_id')
            ->leftJoin('Tasks', 'Tasks.user_id', '=', 'Users.id')
            //->leftJoin('Users as u2', 'u2.id', '=', 'Tasks.user_id')
            ->where('Tasks.user_id', '=', $uid)
            ->get();
    }

    public static function getUserTaskAnswered($uid = 0) {
        return DB::table('Student_TaskAnswers')
            ->select(DB::raw('*'))
            ->where('Student_TaskAnswers.user_id', '=', $uid)
            ->get();
    } */

}