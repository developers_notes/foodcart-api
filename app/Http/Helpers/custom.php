<?php
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use App\Http\Services\Log\ErrorReturn;

if (!function_exists('public_path')) {
   /**
    * Get the path to the public folder.
    *
    * @param  string $path
    * @return string
    */
    function public_path($path = '')
    {
        return env('PUBLIC_PATH', base_path('public')) . ($path ? '/' . $path : $path);
    }
}

    if (!function_exists('custom_getimagesizefromstring')) {
        function custom_getimagesizefromstring($data)
        {
            return (int) (strlen(rtrim($data, '=')) * 3 / 4);
            //$uri = 'data://application/octet-stream;base64,' . base64_encode($data);
            //return getimagesize($uri);
        }
    }

    /**
     * system_email_get_admin method
     * Builds array of objects error
     * @param object : base object to be searched
     * @param string : property name used as a key to search the value
     * @param any : the value to be matched
     * 
     * @return object
     */
    if (!function_exists('custom_system_object_find')) {
        function custom_system_object_find($obj, $prop = 'name', $val = 1)
        {
            foreach($obj as $k => $v) {
                if ($v[$prop] == $val) {
                    return $v;
                    break;
                }
            }
            return false;
        }
    }

    /**
     * Custom file checker size using base64 string.
     * Uploads to the server and checks its file size then delete it.
     * 
     * @param d : base64 File 
     * @param u : User used to create temporary file
     * @param ext : Extention name used to create temporary file
     * 
     * @return object
     */
    if (!function_exists('custom_checkFileSize')) {
        function custom_checkFileSize($d, $u, $ext)
        {
            $v = 'temp_'.$u->id.'.'.$ext;
            $data = file_get_contents( $d );
            $r = Storage::disk('temp_file')->put($v, $data);
            $size =  (Storage::disk('temp_file')->size($v) / 1000) / 1000 ;
            Storage::disk('temp_file')->delete($v);
            return $size;
        }
    }

    /**
     * Convert the javascript date to Mysql Savable DAte
     * 
     * @param d : JS date
     * 
     * @return object
     */
    if (!function_exists('jsToSavableDate')) {
        function jsToSavableDate($d)
        {
            return date('Y-m-d G:i:s', strtotime($d));
        }
    }

    /**
     * Convert the Mysql Date to viewable 
     * 
     * @param d : JS date
     * 
     * @return object
     */
    if (!function_exists('mysqltoViewableDate')) {
        function mysqltoViewableDate($d)
        {
            //return '2020-08-15T11:21:20.605Z';
            return date_format(date_create($d),"Y-m-d H:i:s");
        }
    }

    if (!function_exists('custom_covertToArray')) {
        function custom_covertToArray($d)
        {
            if ($d) {
                $arr = array('option' => array(), 'answer' => array());
                foreach($d as $i => $v) {
                    if ( $v['value'] ) {
                        $arr['option'][] = $v['value'];
                        if ($v['isChecked']===true) $arr['answer'][] = $v['value'];
                    }
                }
                return $arr;
            }
            return null;
        }
    }

    if (!function_exists('getBetween')) {
        function getBetween($pool,$var1="",$var2=""){
            $temp1 = strpos($pool,$var1)+strlen($var1);
            $result = substr($pool,$temp1,strlen($pool));
            $dd=strpos($result,$var2);
            if($dd == 0){
            $dd = strlen($result);
            }
            return substr($result,0,$dd);
        }
    }

    if (!function_exists('randomColors')) {
        function randomColors() {
            return [
                '51A7DD',
                '7b1fa2',
                '679F38',
                'F57C01',
                'E43935',
                'c2185b',
                '7BB342',
                'description',
                '00B0FF',
                'EF5350'
            ];
        }
    }

    if (!function_exists('sendMobile')) {
        function sendMobile($mobile, $msg = "Hello, this is a sample broadcast.") {
            return false;
            // if ($mobile != '09177106739') return true;
            $url = 'https://smsapi.mobile360.ph/v2/api/broadcast';
            $payload = array(
                "username" => "LPG",
                "password" => "wSqW8vts",
                "msisdn" => $mobile,
                "content" => $msg,
                "shortcode_mask" => "GASLINE LPG",
                "is_intl" => false,
            );
            try {
                $client = new Client();
                $response = $client->request('POST', $url, array('form_params' => $payload));
                return json_decode($response->getBody()->getContents());
            } catch(Exception $e) {
                $errorObj = new ErrorReturn();
                $errorObj->set('SERVER_ERROR', $e);
                return response()->json($errorObj->get(), $errorObj->code());
            }
        }
    }

    /**
     * Anticipate the next date of order.
     * Requires atleast 3 transction to compute
     * 
     * @param array
     * 
     * @return date
     */
    if (!function_exists('compute_next_order')) {
        function compute_next_order($d = array()) {
            if (count($d) <= 2) return false;
            
            $origin = new DateTime( date('Y-m-d', strtotime($d[0]['created_at'])) );
            $target = new DateTime( date('Y-m-d', strtotime($d[ count($d)-1 ]['created_at'])) );
            $interval = $origin->diff($target);
            $ret = getBetween($interval->format('%R%a days'), '+', ' days');
            if ( is_numeric($ret) ) {
                return $ret;
            }
            return false;
        }
    }

    if (!function_exists('dateNow')) {
        function dateNow() {
            return date('Y-m-d h:i:s');
        }
    }

    if (!function_exists('setLogFile')) {
        function setLogFile($content, $isnew = false, $file = 'log.txt') {
            if ($isnew) {
                Storage::disk('local')->put('temporary/'.$file, $content);
            } else {
                Storage::disk('local')->append('temporary/'.$file, $content);
            }
        }
    }

    