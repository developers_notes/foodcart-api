<?php
namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\Users;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use App\Http\Services\Log\ErrorReturn;
use Illuminate\Http\Request;

class JwtMiddleware
{
    public function __construct() {
        $this->errorObj = new ErrorReturn();
    }

    /**
     * JWT request hanlder.
     * 
     * @return json Description of the return value, which tak
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('Authorization');
        $ipToken = 'token_' . $request->server->get('REMOTE_ADDR');
        if(!$token) {
            // Unauthorized response if token not there
            $this->errorObj->set('MISMATCH_TOKEN');
            return response()->json($this->errorObj->get(), $this->errorObj->code());
        }
        $token = explode(' ', $token);
        $token = $token[1];
        return $next($request); // do not load the logic for token handling

        // setLogFile($request->server->get('REMOTE_ADDR') . ' ======: ' . $token);
        if (app('redis')->exists($ipToken)) {
            $token = app('redis')->get($ipToken);
            $user = Users::where('token', '=', $token)->first();
            if (isset($user->id)) {
                $request->auth = $user;
                return $next($request);
            }
        }
        $this->errorObj->set('NOTFOUND_TOKEN');
        return response()->json($this->errorObj->get(), $this->errorObj->code());
        
        try {
            $credentials = JWT::decode($token,   ('JWT_SECRET'), ['HS256']);
            $user = Users::find($credentials->sub);
            
            // jms - added exception for logout no matter what happen to token, it should be forcely signed out
            if(!in_array('logout', $request->segments()) && $user->token !== $token) {
                $this->errorObj->set('INVALID_TOKEN');
                return response()->json($this->errorObj->get(), $this->errorObj->code());
            }
        } catch(ExpiredException $e) {
            $tkn = explode('.', $token);
            $tkn_decoded = json_decode(base64_decode($tkn),true);
            
            $this->errorObj->set('EXPIRED_TOKEN'); 
            return response()->json($this->errorObj->get(), $this->errorObj->code());
        } catch(Exception $e) {
            $this->errorObj->set('INVALID_TOKEN', $e);
            return response()->json($this->errorObj->get(), $this->errorObj->code());
        }
        $user = Users::find($credentials->sub);
        // Now let's put the user in the request class so that you can grab it from there
        $request->auth = $user;
        return $next($request);
    }
}