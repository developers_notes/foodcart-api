<?php 

namespace App\Http\Services\Token;

/**
 * Token Interface.
 */
interface JwtTokenInterface
{
    public function set_token($data);
}