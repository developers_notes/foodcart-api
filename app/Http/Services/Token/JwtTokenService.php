<?php
/**
 * Service class SetToken | app/Http/Services/Tokem/SetToken.php
 *  
 * @author      Jaypee Sanchez <developers_notes@yahoo.com>
 */
namespace App\Http\Services\Token;

use Firebase\JWT\JWT;
use Firebase\JWT\JWTAuth;
use Firebase\JWT\ExpiredException;
use Illuminate\Database\Eloquent\Model;

use App\Http\Services\Token\JwtTokenInterface as JwtTokenInterfaceConfig;

/**
 * Class Service - Set token
 * 
 * This service class used to set template for email notification transactions. 
 */
class JwtTokenService implements JwtTokenInterfaceConfig
{
    /**
     * @var $model will take array of data from a model.
     */
    protected $model;

    /**
     * Generate JWT token.
     * 
     * @param   $model      Description of this parameter, which takes a laravel model of user.
     * @param   $expiration Description of this parameter, which set a default expiration value equivalent to 1 day.
     * 
     * @return  string      Description of the return value, which is a PHP string value.
     */
    public function set_token($model,$expiration = 1440, $opt = NULL) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $model->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*$expiration // Expiration time: 60 secs * $exipiration secs
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

}