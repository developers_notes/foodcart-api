<?php

/**
 * Service class ErrorReturn | app/Services/Logs/ErrorReturn.php
 * 
 * @author      Jaypee Sanches <Jaypee.Sanchez@erni.ph>
 */

namespace App\Http\Services\Log;
use Log;

/**
 * Class Service - ErrorReturn
 * 
 * The API is designed to be integrated across different clients. Therefore return error must be agnostic.
 * The error of the api must be completely defined si client will have the freedom to
 * customize their own error messages.
 * 
 * ErrorReturn has a capability to have a multiple error 
 * `$obj = new ErrorReturn();`
 * `$obj->set('ERROR1');`
 * `$obj->set('ERROR2'); `
 *
 */
class ErrorReturn
{
    /**
     * ErrorLogs class.
     *
     * This is where the Error constants are defined
     * 
     */

    const SUCCESS               = array('code' => 200, 'type' => 'SUCCESS', 'message' => 'Success' );
    const LOGIN_FAILED          = array('code' => 400, 'type' => 'LOGIN_FAILED', 'message' => 'Login Failed' );
    const INVALID_TOKEN         = array('code' => 400, 'type' => 'INVALID_TOKEN', 'message' => 'Invalid Token' );
    const EXPIRED_TOKEN         = array('code' => 400, 'type' => 'EXPIRED_TOKEN', 'message' => 'Provided token is expired' );
    const NOTFOUND_TOKEN        = array('code' => 400, 'type' => 'NOTFOUND_TOKEN', 'message' => 'Provided token is not found' );
    const MISMATCH_TOKEN        = array('code' => 400, 'type' => 'MISMATCH_TOKEN', 'message' => 'Provided token could not be found in DB' );
    const BLOCKED_ACCOUNT       = array('code' => 400, 'type' => 'BLOCKED_ACCOUNT', 'message' => 'Account is Blocked' );
    const USERNAME_PASS_INC     = array('code' => 400, 'type' => 'USERNAME_PASS_INC', 'message' => 'Username / Password Incorrect' );
    const USERNAME_PASS_REQ     = array('code' => 400, 'type' => 'USERNAME_PASS_REQ', 'message' => 'Username / Password required' );
    const PASS_INC              = array('code' => 400, 'type' => 'PASS_INC', 'message' => 'Password Incorrect' );
    const FORM_ERROR            = array('code' => 400, 'type' => 'FORM_ERROR', 'message' => 'Form Error' );
    const INSERT_ERROR          = array('code' => 400, 'type' => 'INSERT_ERROR', 'message' => 'Insert error' );
    const FORM_PASS_REQ         = array('code' => 400, 'type' => 'FORM_PASS_REQ', 'message' => 'Password is required' );
    const USER_ATTENDANCE_FOUND = array('code' => 400, 'type' => 'USER_ATTENDANCE_FOUND', 'message' => 'User attendance already submitted' );
    const EMPTY_MODEL           = array('code' => 404, 'type' => 'EMPTY_MODEL', 'message' => 'Empty data collections' );
    const ITEM_NOTFOUND         = array('code' => 404, 'type' => 'ITEM_NOTFOUND', 'message' => 'Item not found' );
    const USER_NOTFOUND         = array('code' => 404, 'type' => 'USER_NOTFOUND', 'message' => 'Account does not exist' );
    const EMAIL_NOTFOUND        = array('code' => 404, 'type' => 'EMAIL_NOTFOUND', 'message' => 'E-mail does not exist' );
    const TASK_NOTFOUND         = array('code' => 404, 'type' => 'TASK_NOTFOUND', 'message' => 'Task(s) not found' );
    const OPERATION_PERM        = array('code' => 403, 'type' => 'OPERATION_PERM', 'message' => 'Not authorize' );
    const ACCESS_PERM           = array('code' => 403, 'type' => 'ACCESS_PERM', 'message' => 'Access not granted' );
    const FILE_NOT_ALLOWED      = array('code' => 404, 'type' => 'FILE_NOT_ALLOWED', 'message' => 'File not allowed' );
    const SERVER_ERROR          = array('code' => 500, 'type' => 'SERVER_ERROR', 'message' => 'Server Error' );

    /**
     * Set model static variable.
     * @var $errors  will take array of errors details.
     */
    public $errors = [];

    public function __construct(){

    }

    /**
     * Set error message
     * 
     * @param   $val Description of this parameter, which takes a PHP string value of ErrorReturn class constant.
     * @param   $details Description of this parameter, which takes a PHP string value of any error details.  
     * @return  object Description of the return value, which is request reponse to delete kanton.
     */
    public function set($val, $details = null) {
        // unset existing $errors
        // unset($this->errors);
        $this->errors[] = constant("self::$val"); // self::USERNAME_PASS_INC
        if( $details !== null ) {
            $this->errors[0]['details'] = $details->getMessage();
            if ( strpos($details->getMessage(), 'Duplicate entry') ) {
                $this->errors[0]['details'] = getBetween($details->getMessage(), 'Integrity constraint', 'for key');
            }
            
        }
    }

    /**
     * Get errors
     *  
     * @return  object Description of the return value, which is array of errors.
     */
    public function get() {
        return $this->errors;
    }

    /**
     * Get error code
     *  
     * @return  object Description of the return value, which is code number of the error.
     */
    public function code() {
        return $this->get()[0]['code'];
    }
    
}