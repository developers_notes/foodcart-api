<?php
/**
 * Service class SetToken | app/Http/Services/Tokem/SetToken.php
 *  
 * @author      Jaypee Sanchez <developers_notes@yahoo.com>
 */
namespace App\Http\Services\General;



/**
 * Class Service - Set token
 * 
 * This service class used to set template for email notification transactions. 
 */
class SampleService
{
    /**
     * @var $model will take array of data from a model.
     */

    public function getSampleContent() {
        return "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
        when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
        It has survived not only five centuries, but also the leap into electronic typesetting, remaining 
        essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing 
        Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including 
        versions of Lorem Ipsum.";
    }

    public function getSampleWysiwygContent() {
        return `<p style="text-align: center;"><span style="font-size: 14px;">&rarr; This is a full-featured demo. Try it and see for yourself!&nbsp;</span><span style="font-size:14px;">&larr;</span></p>

        <p style="text-align: center;">
            <br><span style="font-size: 24px;"><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif;">Froala Editor is a lightweight WYSIWYG HTML Editor written in Javascript that enables rich text editing capabilities for your applications.</span></span><span class="fr-img-caption fr-dib fr-rounded fr-shadow" style="width: 203px;"><span class="fr-img-wrap"><img src="https://froala.com/wp-content/themes/jupiterx-child/assets/images/editor/pages/v3/editor-photo.png" alt="Editor photo"><span class="fr-inner">This is an image caption.</span></span></span></p>
        
        <table class="fr-dashed-borders fr-alternate-rows" style="width: 100%;">
            <tbody>
                <tr>
                    <td style="text-align: center;"><span style="font-size: 24px;">Complete<a href="https://froala.com/wysiwyg-editor/docs/" title="Documentation">&nbsp;<strong>documentation</strong></a>&nbsp;</span><span class="fr-emoticon fr-deletable fr-emoticon-img" style='background: url("https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/1f4da.svg"); font-size: 24px;'>&nbsp;</span>
                        <br>
                    </td>
                    <td style="text-align: center;"><span style="font-size: 24px;">Framework <strong><a href="https://froala.com/wysiwyg-editor/docs/framework-plugins/">plugins</a></strong>&nbsp;</span><span class="fr-emoticon fr-deletable fr-emoticon-img" style='background: url("https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/1f50c.svg"); font-size: 24px;'>&nbsp;</span>
                        <br>
                    </td>
                    <td style="text-align: center;"><span style="color: rgb(0, 0, 0); font-size: 24px;">Tons of&nbsp;</span><strong><span style="font-size: 24px;"><a href="https://froala.com/wysiwyg-editor/examples/">examples</a>&nbsp;</span></strong><span class="fr-emoticon fr-deletable fr-emoticon-img" style='background: url("https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/1f320.svg"); font-size: 24px;'>&nbsp;</span>
                        <br>
                    </td>
                </tr>
            </tbody>
        </table>
        `;
    }

}