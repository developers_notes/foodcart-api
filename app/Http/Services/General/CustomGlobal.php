<?php
/**
 * Service class SetToken | app/Http/Services/Tokem/SetToken.php
 *  
 * @author      Jaypee Sanchez <developers_notes@yahoo.com>
 */
namespace App\Http\Services\General;



/**
 * Class Service - Set token
 * 
 * This service class used to set template for email notification transactions. 
 */
class CustomGlobal
{
    /**
     * @var $model will take array of data from a model.
     */
    private $d;

    public function __construct($d) {
        $this->d = $d;
    }
    
    public static function jsToUnix($d) {
        return strtotime(date($d));
    }

    public function mysqltoViewableDate() {
        return date('Y-m-d h:i A', strtotime(date($this->d)));
    }
    
    public function jsToSavableDate() {
        return date('Y-m-d G:i:s', strtotime(date($this->d)));
    }

    public function mysqltoViewableTime() {
        return date('h:i A', strtotime(date($this->d)));
    }

    public function mysqltoToViewableDay() {
        return date('Y-m-d', strtotime(date($this->d)));
    }

    public function mysqlDateToUnix() {
        return strtotime($this->d);
    }

    public static function public_path() {
        return 'test';
    }

    

}