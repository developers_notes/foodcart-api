<?php
/**
 * Service class SetToken | app/Http/Services/Tokem/SetToken.php
 *  
 * @author      Jaypee Sanchez <developers_notes@yahoo.com>
 */
namespace App\Http\Services\General;
use Validator;

/**
 * Class Service - Set token
 * 
 * This service class used to set template for email notification transactions. 
 */
class CustomValidator
{
    /**
     * @var $model will take array of data from a model.
     */
    public function __construct() {}

    public static function load() {
        
        Validator::extend('contains', function($attribute, $value, $parameters) {
            // Banned words
            $words = array('jpg', 'png', 'pdf');
            foreach ($words as $word)
            {
                if (stripos($value, $word) !== false) return true;
            }
            return false;
        });
    }
    

}