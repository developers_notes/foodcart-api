<?php
/**
 * Users Controller | app/Http/Controllers/UsersController.php
 *
 * @author      Jaypee Sanches <developers_notes@yahoo.com>
 */
namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\Users;
use App\Http\Services\Log\ErrorReturn;
use App\Http\Services\Token\JwtTokenService;
use DB;
use Validator;

/**
 * Class - users controller
 *
 * View, edit and create.
 */
class UsersController extends BaseController
{  
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request = NULL)
    {
        $this->errorObj = new ErrorReturn();
        if ($request !== NULL && !in_array('login', $request->segments())) {
            // $this->user = Users::getCurrentUser($request);
        }
    }

    /**
     * Show single or all users.
     *
     * @param   $id     Description of this parameter, which takes a PHP int value of users id.
     * @return  object  Description of the return value, which is request reponse list or single logs.
     */
    public function get($id = NULL, Request $request = NULL) {
        $this->errorObj->set('USER_NOTFOUND');
        if ($id) {
            if ( $model = Users::where('username', '=', $id)->first() ) {
                return response()->json($model);
            }
            return response()->json($this->errorObj->get(), $this->errorObj->code());
        } else {
            $model = new Users;
            $page = 0;
            $position =  'ASC';
            $type =  'id';
            // raad - if request has page parameter, set page value
            if( $request->has('page') ) {
                $page = $request->input('page');
            }
            if($request->input() != []) {
                // // set sort position
                if($request->has('sort_position')) {
                    $position = $request->input('sort_position');
                }

                if($request->has('type')) {
                    if( $request->input('type') == 'date' ) {
                        $type = 'created_at';
                    } else {
                        $type = $request->input('type');
                    }
                }
            }

            $model = $model->orderBy($type, $position);
            return response()->json($model->get());
        }
    }

    /**
     * Save new or update users.
     *
     * @param   $id         Description of this parameter, which takes a PHP int value of logs id.
     * @param   $request    Description of this parameter, which takes a PHP array value of the request made.  
     * @return  object      Description of the return value, which is request reponse list or single logs.
     */
    public function save($id = NULL, Request $request = NULL) {
        $rules = array('username' => 'required', 'email' => 'required');
        $validator = Validator::make($request->data, $rules);
        if ($id) {
            $update = array();
            // Get current set model from setException.
            try {
                if ( isset($validator->errors()->all()[0]) ) {
                    throw new Exception( $validator->errors()->all()[0] );
                }
                $user = Users::where('id', '=', $id);
                // Create user 
                foreach( $request->data as $key => $value ) {
                    if($key === 'password' && $value) { 
                        $update[$key]= Hash::make($value);
                    } else if ($key === 'firstname' && empty($value)) {
                        $update[$key] = 'n/a';
                    } else if ($key === 'lastname' && empty($value)) {
                        $update[$key] = 'n/a';
                    } else {
                        $update[$key] = $value;
                        if ($key === 'password' && !$value) {
                            unset($update['password']);
                        }
                    }
                }
                // Save Company document
            return response()->json( $user->update($update) );
            } catch(Exception $e) {
                $this->errorObj->set('SERVER_ERROR', $e);
                return response()->json($this->errorObj->get(), $this->errorObj->code());
            }

        } else {
            try {
                if ( isset($validator->errors()->all()[0]) ) {
                    throw new Exception( $validator->errors()->all()[0] );
                }
                $user_model = new Users();
                foreach( $request->data as $key => $value ) {
                    if ($key == "command" || $key == "hashedPassword") continue;

                    // jms insert item here where user with specific privilege can only update a specific table/column
                    if($key === 'password') {
                        $user_model->{$key} = $value;
                    } else if ($key === 'firstname' && empty($value)) {
                        $user_model->{$key} = 'n/a';
                    } else if ($key === 'lastname' && empty($value)) {
                        $user_model->{$key} = 'n/a';
                    } else {
                        $user_model->{$key} = $value;
                        if ($key == 'code') {
                            $user_model->{$key} = strtoupper($com->code);
                        }
                    }
                }

                
                //$user_model->username = $username;
                //$user_model->password = Hash::make($username);
                // throw new Exception('Data is not Provided!');
                return response()->json( $user_model->save() );

            } catch(Exception $e) {
                $this->errorObj->set('SERVER_ERROR', $e);
                return response()->json($this->errorObj->get(), $this->errorObj->code());
            }
        }
        return response()->json($this->errorObj->get(), $this->errorObj->code());
    }

    public function changepassword($id = NULL, Request $request = NULL){
        try {
            if ($request->has('data') && isset($request->get('data')['oldpassword'])) {
                $user = Users::where('username', '=', $this->user->username)->first();
                if ($user && Hash::check($request->get('data')['oldpassword'], $user->password)) {
                    return response()->json( Users::where('id', '=', $this->user->id)->update(
                        ['password' => Hash::make($request->get('data')['password'])]
                    ));
                } else {
                    $this->errorObj->set('PASS_INC');
                }
            }  else {
                $this->errorObj->set('SERVER_ERROR');
            }
            return response()->json($this->errorObj->get(), $this->errorObj->code());
        } catch(Exception $e) {
            $this->errorObj->set('SERVER_ERROR', $e);
            return response()->json($this->errorObj->get(), $this->errorObj->code());
        } 
    }

    /**
     * Get Timzones
     * 
     * @return object Description of the return value, which is request reponse list or single logs.
     */
    public function timezone() {
        $tz = Timezone::get();
        ksort($tz);
        return $tz;
    }

    public function login(Request $request = NULL) {
        if (!$request->has('username') || !$request->has('password')) {
            $this->errorObj->set('USERNAME_PASS_REQ');
        }
        if ($request->has('username') && $request->has('password')) {
            $user = Users::where('username', '=', $request->username)->first();
            if ($user && Hash::check($request->password, $user->password)) {
                $jwtToken = new JwtTokenService($user);
                $token = $jwtToken->set_token($user, 1440);
                app('redis')->set('token_' . $request->server->get('REMOTE_ADDR'), $token);
                Users::where('id', '=', $user->id)->update(['login_attempt'=>0,'last_login'=>dateNow(), 'token' => $token, 'reset_token' => $token]);
                return response()->json( [
                    'token' => $token,
                    'user' => $user->id,
                    'name' => $user->firstname . ' ' . $user->lastname,
                    'role' => $user->role,
                ] );
            }
            $this->errorObj->set('USERNAME_PASS_INC');
        }
        
        return response()->json($this->errorObj->get(), $this->errorObj->code());
    }

    public function logout(Request $request = NULL) {
        try {
            app('redis')->del('token_' . $request->server->get('REMOTE_ADDR'));
            return response()->json(Users::where('id', '=', $this->user->id)->update(['login_attempt'=>0, 'token' => NULL, 'reset_token' => NULL]));
        } catch(Exception $e) {
            $this->errorObj->set('SERVER_ERROR', $e);
            return response()->json($this->errorObj->get(), $this->errorObj->code());
        }
    }
    


}
