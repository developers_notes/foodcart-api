<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
const V1 = 'api/v1';
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => V1.'/user', 'middleware'=>'jwt.auth'], function($router) {
    /** GET REQUEST */
    $router->get('/', 'UsersController@get');
    $router->get('/{id}', 'UsersController@get');
    
    $router->post('/', 'UsersController@save');
    $router->put('/{id}', 'UsersController@save');
});