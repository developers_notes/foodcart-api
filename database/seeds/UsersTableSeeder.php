<?php

use Illuminate\Database\Seeder;
use App\Models\Bgy;
use App\Models\Area;
use App\Models\Communities;
use App\Models\CustomerTypes;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $dateNow = dateNow();
        \App\Models\Users::insert([
            [ 
                'displayName' => 'Luffy',
                'username' => 'admin',
                'password' => Hash::make('admin'),
                'email' => 'developers_notes@yahoo.com',
                'token' => 'thisisatesttokentobechagef',
                'reset_token' => 'thisisatestreset_tokentobechagef',
                'created_at' => $dateNow, 'updated_at' => $dateNow,
            ]
        ]);
    }
}
