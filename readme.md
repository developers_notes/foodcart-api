# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Disclaimer
    I used WSL for BE, This is is also compatible windows because the whole BE is derived from one of my client
    

## Direction

1. run: composer install
2. Run query on your workbench / phpmyqdmin / command : CREATE DATABASE exam
3. migrate table(s) needed by running : php artisan migrate:fresh
4. Sometimes cache needs to refresh if it doesn't work so run: composer dump-autoload
5. Run seeder: php artisan db:seed

6. When running linux / wsl  (optional)
    Make sure that the /var/www/[PROJECTNAME ]/storage folder has the right ownership and chmod must be 777. Also it's subfolders

7. create .env file
    must be present in the root folder
        DB_CONNECTION=mysql
        DB_HOST=[YOUR MYSQL HOST]
        DB_PORT=3306
        DB_DATABASE=exam
        DB_USERNAME=[YOUR MYSQL USERNAME]
        DB_PASSWORD=[YOUR MYSQL PASSWORD]

        CACHE_DRIVER=file
        QUEUE_CONNECTION=sync
        JWT_SECRET=[WHAT EVER YOU WISH RANDOM STRING]

8. import to postman:

{
	"info": {
		"_postman_id": "53a177b1-4f96-4434-8e1b-36553b191adf",
		"name": "Exam",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
	},
	"item": [
		{
			"name": "GET",
			"description": "",
			"item": [
				{
					"name": "Get All User",
					"request": {
						"method": "GET",
						"header": [
							{
								"key": "Authorization",
								"value": "Bearer {{token}}"
							}
						],
						"body": {},
						"url": {
							"raw": "{{host}}/user",
							"host": [
								"{{host}}"
							],
							"path": [
								"user"
							]
						},
						"description": "GET All User"
					},
					"response": []
				},
				{
					"name": "Get Specific User",
					"request": {
						"method": "GET",
						"header": [
							{
								"key": "Authorization",
								"value": "Bearer {{token}}"
							}
						],
						"body": {},
						"url": {
							"raw": "{{host}}/user/1",
							"host": [
								"{{host}}"
							],
							"path": [
								"user",
								"1"
							]
						},
						"description": "GET All User"
					},
					"response": []
				}
			]
		}
	]
}

9. To check if BE is working please use postman:
    Type: GET
    url: [host]/api/v1/user/1 or [host]/api/v1/user

